//
//  AppCoordinator.swift
//
//  Created by Nick Melnick on 18.02.2019.
//  Copyright © 2019 Nick Melnick. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: BaseCoordinator {
    
    private var window: UIWindow!
    
    var audioService = AudioService()
    
    var scheduler: Scheduler!
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)) {
        self.window = window
    }
    
    override func start() {
        self.window.makeKeyAndVisible()
        scheduler = Scheduler(audioService: self.audioService)
        showMainScreen()
    }
    
    func showMainScreen() {
        self.removeChildCoordinators()
        
        let coordinator = MainScreenCoordinator(audiservice: self.audioService, scheduler: self.scheduler)
        coordinator.navigationController = BaseNavigationController()
        self.start(coordinator: coordinator)
        
        ViewControllerUtils.setRootViewController(
            window: self.window,
            viewController: coordinator.navigationController,
            withAnimation: true
        )
    }
    
}
