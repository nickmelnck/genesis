//
//  AlarmScreenCoordinator.swift
//  Genesis
//
//  Created by Nick Melnick on 10.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

class AlarmScreenCoordinator: BaseCoordinator {
    
    private let viewModel: AlarmScreenViewModel
    
    var alarmViewController: BaseNavigationController!
    
    required init(audioService: AudioService) {
        self.viewModel = AlarmScreenViewModel(audioService: audioService)
    }
    
    override func start() {
        let viewController = AlarmScreenCntrl.instantiateViewController()
        self.viewModel.coordinator = self
        viewController.viewModel = self.viewModel
        
        self.alarmViewController = BaseNavigationController(rootViewController: viewController)
        let segue = CardSegue(identifier: nil, source: self.navigationController, destination: self.alarmViewController)
        self.navigationController.prepare(for: segue, sender: nil)
        segue.perform()
    }
    
    func didChangeAlarm() {
        self.alarmViewController.dismiss(animated: true, completion: nil)
        self.parentCoordinator?.didFinish(coordinator: self)
    }
    
}
