//
//  AlarmScreenCntrl.swift
//  Genesis
//
//  Created by Nick Melnick on 10.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

class AlarmScreenCntrl: UIViewController {
    
    var viewModel: AlarmScreenViewModel? {
        didSet {
            guard let vm = viewModel else { return }
            self.isAlarmEnabled = vm.isAlarmEnabled
            self.alarmTime = vm.alarmTime
        }
    }
    
    var isAlarmEnabled: Bool = true
    var alarmTime: TimeInterval = 0

    @IBOutlet var lblAlarmPrompt: UILabel!
    @IBOutlet var swAlarmEnabled: UISwitch!
    
    @IBOutlet var dpAlarmPicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        configureText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateContent()
    }
    
    private func configureUI() {
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction(_:)))
        self.navigationItem.rightBarButtonItem = btnDone
        let btnCancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction(_:)))
        self.navigationItem.leftBarButtonItem = btnCancel
    }
    
    private func configureText() {
        lblAlarmPrompt.text = "Alarm enabled".localized
        self.title = "Alarm".localized
    }
    
    private func updateContent() {
        self.swAlarmEnabled.isOn = self.isAlarmEnabled
        self.dpAlarmPicker.date = Date(timeIntervalSince1970: alarmTime)
    }
    
    
    @IBAction private func doneAction(_ sender: Any?) {
        self.viewModel?.setAlarm(enabled: self.isAlarmEnabled, time: self.alarmTime)
    }

    @IBAction private func cancelAction(_ sender: Any?) {
        guard let vm = self.viewModel else { return }
        self.viewModel?.setAlarm(enabled: vm.isAlarmEnabled, time: vm.alarmTime)
    }
    
    @IBAction func alarmEnabledValueChanged(_ sender: UISwitch) {
        self.isAlarmEnabled = sender.isOn
    }
    @IBAction func timePickerValueChanged(_ sender: UIDatePicker) {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: sender.date)
        let minutes = calendar.component(.minute, from: sender.date)
        if let newTime = Calendar.current.date(bySettingHour: hour, minute: minutes, second: 0, of: Date(), matchingPolicy: .nextTime, repeatedTimePolicy: .first, direction: .forward) {
            self.alarmTime = newTime.timeIntervalSince1970
        }
    }
    
}

extension AlarmScreenCntrl: StoryboardProtocol {
    
    static var storyboardName: String {
        return "Main"
    }
    
}
