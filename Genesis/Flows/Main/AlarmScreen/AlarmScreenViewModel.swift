//
//  AlarmScreenViewModel.swift
//  Genesis
//
//  Created by Nick Melnick on 10.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

class AlarmScreenViewModel {
    
    weak var coordinator: AlarmScreenCoordinator!
    
    private unowned var audioService: AudioService!
    
    init(audioService: AudioService) {
        self.audioService = audioService
    }
    
    var isAlarmEnabled: Bool {
        return audioService.isAlarmEnabled
    }
    
    var alarmTime: TimeInterval {
        return audioService.alarmTime
    }
    
    func setAlarm(enabled: Bool, time: TimeInterval) {
        self.audioService.isAlarmEnabled = enabled
        self.audioService.alarmTime = time
        coordinator?.didChangeAlarm()
    }
    
}


