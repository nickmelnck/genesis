//
//  MainScreenCoordinator.swift
//  Genesis
//
//  Created by Nick Melnick on 08.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

class MainScreenCoordinator: BaseCoordinator {
    
    private let viewModel: MainScreenViewModel
    
    private let audioService: AudioService
    
    required init(audiservice: AudioService, scheduler: Scheduler) {
        self.audioService = audiservice
        self.viewModel = MainScreenViewModel(audioService: audiservice, scheduler: scheduler)
        super.init()
    }
    
    override func start() {
        let viewController = MainScreenCntrl.instantiateViewController()
        self.viewModel.coordinator = self
        viewController.viewModel = self.viewModel
        
        self.navigationController.isNavigationBarHidden = true
        self.navigationController.viewControllers = [viewController]
    }
    
    func setAlarmTime() {
        let coordinator = AlarmScreenCoordinator(audioService: self.audioService)
        coordinator.navigationController = self.navigationController
        self.start(coordinator: coordinator)
    }
    
}
