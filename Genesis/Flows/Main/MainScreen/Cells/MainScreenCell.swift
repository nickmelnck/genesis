//
//  MainScreenCell.swift
//  Genesis
//
//  Created by Nick Melnick on 09.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

class MainScreenCell: UITableViewCell, RegisterCellProtocol {
    
    var isActive = true {
        didSet {
            contentView.backgroundColor = UIColor(hex: "1A72FF").withAlphaComponent(isActive ? 0.5 : 0.15)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.textLabel?.textColor = Constants.UI.lblText
        self.detailTextLabel?.textColor = Constants.UI.lblParam
        contentView.backgroundColor = UIColor(hex: "1A72FF").withAlphaComponent(isActive ? 0.5 : 0.15)
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
