//
//  MainScreenViewModel.swift
//  Genesis
//
//  Created by Nick Melnick on 08.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation

protocol MainScreenViewModelDelegate: class {
    func didUpdateParams(_ viewModel: MainScreenViewModel)
}

class MainScreenViewModel {
    
    weak var delegate: MainScreenViewModelDelegate?
    
    weak var coordinator: MainScreenCoordinator!
    
    private unowned var audioService: AudioService!
    private unowned var scheduler: Scheduler!
    
    private lazy var timeFormatter: DateComponentsFormatter = {
        $0.unitsStyle = .brief
        $0.allowedUnits = [.minute, .hour]
        $0.zeroFormattingBehavior = .dropAll
        $0.maximumUnitCount = 2
        return $0
    }(DateComponentsFormatter())
    
    private lazy var dateFormatter: DateFormatter = {
        $0.dateStyle = .none
        $0.timeStyle = .short
        return $0
    }(DateFormatter())
    
    init(audioService: AudioService, scheduler: Scheduler) {
        self.audioService = audioService
        self.audioService.add(delegate: self)
        self.scheduler = scheduler
    }
    
    deinit {
        self.audioService.remove(delegate: self)
    }
    
    var state: AudioServiceState {
        return audioService.state
    }
    
    var calmingSoundDuration: String {
        return audioService.calmingSoundDuration.asDurationString ?? "Off".localized
    }
    
    var isCalmingSoundEnabled: Bool {
        return audioService.isCalmingSoundEnabled
    }
    
    var isTrackingEnabled: Bool {
        return audioService.isTrackingEnabled
    }
    
    var isAlarmEnabled: Bool {
        return audioService.isAlarmEnabled
    }
    
    var alarmTime: String {
        return audioService.alarmTime.asTimeString
    }
    
    //MARK: Change Params
    func setCalmingDuration(_ duration: TimeInterval) {
        audioService.calmingSoundDuration = duration
    }
    
    func toggleTrackingActive() {
        audioService.isTrackingEnabled = !audioService.isTrackingEnabled
    }
    
    //MARK: Session Methods
    
    func toggleState() {
        if audioService.state == .idle {
            if audioService.isAlarmEnabled {
                scheduler.registerPushNofications(success: { [weak self] in
                    self?.scheduleAlarm()
                })
            }
        }
        switch audioService.state {
        case .idle, .pause:
            audioService.startSleepSession()
        case .calming, .tracking, .sleep:
            audioService.pauseSleepSession()
        case .alarm:
            // stop alarm
            break
        }
    }
    
    func setAlarmTime() {
        self.coordinator.setAlarmTime()
    }

    
    private func scheduleAlarm() {
        let date = Date(timeIntervalSince1970: audioService.alarmTime)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour, .minute], from: date)
        if let alarmDate = calendar.nextDate(after: Date(), matching: components, matchingPolicy: .nextTime) {
            self.scheduler.scheduleAlarm(withSound: audioService.alarmSound, toDate: alarmDate)
        }
    }
}

extension MainScreenViewModel: AudioServiceDelegate {
    
    func didChangeState(service: AudioService) {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.delegate?.didUpdateParams(self)
        }
    }
    
    func didParamsChanged(service: AudioService) {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.delegate?.didUpdateParams(self)
        }
    }
    
}

