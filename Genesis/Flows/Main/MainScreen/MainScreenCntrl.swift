//
//  MainScreenCntrl.swift
//  Genesis
//
//  Created by Nick Melnick on 08.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

class MainScreenCntrl: UIViewController {
    
    @IBOutlet var cnstrSettingsHeight: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnStart: UIButton!
    @IBOutlet var lblState: UILabel!
    
    var viewModel: MainScreenViewModel? {
        didSet {
            guard let vm = viewModel else { return }
            vm.delegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerNib(MainScreenCell.self)
        configureUI()
        configureText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateContent()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        cnstrSettingsHeight.constant = tableView.contentSize.height
    }
    
    private func configureUI() {
        tableView.backgroundColor = .clear
        btnStart.backgroundColor = Constants.UI.btnColor
        btnStart.layer.cornerRadius = 6
        btnStart.tintColor = .clear
    }
    
    private func configureText() {
        btnStart.setTitle("Start".localized, for: .normal)
        btnStart.setTitle("Pause".localized, for: .selected)
        btnStart.setTitle("Nothing to do".localized, for: .disabled)
    }
    
    private func updateContent() {
        guard let vm = self.viewModel, self.isViewLoaded == true else { return }
        self.lblState.text = vm.state.asString
        self.btnStart.isSelected = (vm.state == AudioServiceState.calming || vm.state == AudioServiceState.tracking)
        self.tableView.reloadData()
        self.btnStart.isEnabled = vm.isAlarmEnabled || vm.isTrackingEnabled || vm.isCalmingSoundEnabled
    }
    
    @IBAction func startPauseAction(_ sender: Any) {
        self.viewModel?.toggleState()
    }
    
    fileprivate func selectCalmingDuration() {
        let ai = UIAlertController(
            title: "Calming Sounds Duration".localized,
            message: nil,
            preferredStyle: .actionSheet
        )
        let list: [TimeInterval] = [0, 1, 5, 10, 15, 20].map{ $0 * 60 }
        list.forEach { (value) in
            var actionTitle: String?
            if value == 0 {
                actionTitle = "Off".localized
            } else {
                actionTitle = value.asDurationString
            }
            ai.addAction(UIAlertAction(title: actionTitle,
                                       style: value == 0 ? .destructive : .default) { [unowned self] _ in
                                    self.viewModel?.setCalmingDuration(value)
            })
        }
        ai.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        self.present(ai, animated: true, completion: nil)
    }
        
}

extension MainScreenCntrl: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MainScreenCell
        if let vm = self.viewModel {
            switch indexPath.section {
            case 0:
                cell.textLabel?.text = "Calming sounds".localized
                cell.detailTextLabel?.text = vm.isCalmingSoundEnabled ? vm.calmingSoundDuration : "Off".localized
                cell.isActive = vm.isCalmingSoundEnabled
            case 1:
                cell.textLabel?.text = "Snore & sleep tracking".localized
                cell.detailTextLabel?.text = vm.isTrackingEnabled ? "Active".localized : "Inactive".localized
                cell.isActive = vm.isTrackingEnabled
            case 2:
                cell.textLabel?.text = "Alarm".localized
                cell.detailTextLabel?.text = vm.isAlarmEnabled ? vm.alarmTime : "Off".localized
                cell.isActive = vm.isAlarmEnabled
            default:
                break
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            selectCalmingDuration()
        case 1:
            viewModel?.toggleTrackingActive()
        case 2:
            viewModel?.setAlarmTime()
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
}

extension MainScreenCntrl: MainScreenViewModelDelegate {
    func didUpdateParams(_ viewModel: MainScreenViewModel) {
        updateContent()
    }
}

extension MainScreenCntrl: StoryboardProtocol {
    
    static var storyboardName: String {
        return "Main"
    }
    
}
