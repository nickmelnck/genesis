//
//  SoundsLibrary.swift
//  Genesis
//
//  Created by Nick Melnick on 10.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation

enum SoundType {
    case calming
    case alarm
}

struct SoundFile {
    
    let name: String
    let soundDescription: String
    let filename: String
    let type: SoundType
    let isPremium: Bool
    var isLocked: Bool = true
    
    init(name: String, soundDescription: String, filename: String, type: SoundType, isPremium: Bool = false, isLocked: Bool = false) {
        self.name = name
        self.soundDescription = soundDescription
        self.filename = filename
        self.type = type
        self.isPremium = isPremium
        self.isLocked = isLocked
    }
    
    var url: URL? {
        guard let path = Bundle.main.path(forResource: self.filename, ofType: nil) else { return nil }
        return URL(fileURLWithPath: path)
    }
    
}
