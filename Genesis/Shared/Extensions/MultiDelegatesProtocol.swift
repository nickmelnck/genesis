//
//  MultiDelegatesProtocol.swift
//
//  Created by Nick Melnick on 18.02.2019.
//  Copyright © 2019 Nick Melnick. All rights reserved.
//

import Foundation

public struct Weak<T> {
    private weak var _value: AnyObject?
    
    public var value: T? {
        get {
            return _value as? T
        }
        set {
            _value = newValue as AnyObject
        }
    }
    
    public init(value: T) {
        self.value = value
    }
}

protocol MultiDelegates: class {
    
    typealias WeakArray = Array<Weak<AnyObject>>
    
    var delegates: WeakArray { get set }
    
    func add(delegate: AnyObject)
    
    func remove(delegate: AnyObject)
    
}

extension MultiDelegates {

    func add(delegate: AnyObject) {
        if delegates.compactMap({ $0.value }).first(where: { $0 === delegate }) == nil {
            delegates.append(Weak(value: delegate))
        }
    }
    
    func remove(delegate: AnyObject) {
        delegates.removeAll(where: { $0.value === delegate })
    }

}
