//
//  UIViewExtension.swift
//  TowerFox
//
//  Created by Nick Melnick on 3/23/19.
//  Copyright © 2019 TowerFox. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func subviewsRecursive() -> [UIView] {
        return subviews + subviews.flatMap { $0.subviewsRecursive() }
    }
    
    var parentViewController: UIViewController? {
        
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
}
