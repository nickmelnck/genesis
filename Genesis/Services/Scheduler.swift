//
//  Scheduler.swift
//  Genesis
//
//  Created by Nick Melnick on 10.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

enum NotificationCategory: String {
    case alarm = "Category.Alarm"
    
    var notificationCategory: UNNotificationCategory {
        switch self {
        case .alarm: return UNNotificationCategory(identifier: self.rawValue,
                                                   actions: [],
                                                   intentIdentifiers: [],
                                                   options: [])
        }
    }
}

class Scheduler: NSObject {
    
    private let center = UNUserNotificationCenter.current()
    
    private var notificationIdentifier: String?
    
    fileprivate let audioService: AudioService
    
    init(audioService: AudioService) {
        self.audioService = audioService
        super.init()
        self.center.setNotificationCategories([
            NotificationCategory.alarm.notificationCategory
        ])
        self.center.delegate = self
        self.center.removeAllDeliveredNotifications()
        self.center.removeAllPendingNotificationRequests()
    }
    
    func registerPushNofications(success: (() -> Void)?) {
        if #available(iOS 12.0, *) {
//            center.requestAuthorization(options: [.alert, .sound, .badge, .provisional, .criticalAlert]) { (granted, error) in
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                Log.info.log("Notifications request: \(granted)")
                if granted {
                    success?()
                }
            }
        } else {
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                Log.info.log("Notification request: \(granted)")
                if granted {
                    success?()
                }
            }
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func scheduleAlarm(withSound sound: SoundFile, toDate date: Date) {
        let content = UNMutableNotificationContent()
        content.title = "Alarm".localized
        content.subtitle = "Alarm went off".localized
        content.categoryIdentifier = NotificationCategory.alarm.rawValue
        content.sound = createNotificationSound(from: sound)
        content.threadIdentifier = ""
        let timeInterval = date.timeIntervalSince(Date())
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
        self.notificationIdentifier = uuidString
        self.center.add(request)
    }
    
    func removeAlarmNotification() {
        guard let id = self.notificationIdentifier else { return }
        center.removePendingNotificationRequests(withIdentifiers: [id])
        center.removeDeliveredNotifications(withIdentifiers: [id])
    }
    
    private func createNotificationSound(from sound: SoundFile) -> UNNotificationSound {
        if #available(iOS 12, *) {
            return UNNotificationSound(named: UNNotificationSoundName(sound.filename))
//            return UNNotificationSound.criticalSoundNamed(UNNotificationSoundName(sound.filename))
        } else {
            return UNNotificationSound(named: UNNotificationSoundName(sound.filename))
        }
    }
    
}

extension Scheduler: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        Log.debug.log("Scheduler userNotificationCenter willPresent")
        let categoryIdentifier = notification.request.content.categoryIdentifier
        
        switch categoryIdentifier {
        case NotificationCategory.alarm.rawValue:
            if let vc = UIApplication.topViewController() {
                let ai = UIAlertController(title: "Alarm".localized, message: "Alarm went off".localized, preferredStyle: .alert)
                let stopAction = UIAlertAction(title: "Stop".localized, style: .destructive) { [weak self] (_) in
                    self?.audioService.pauseSleepSession()
                }
                ai.addAction(stopAction)
                vc.present(ai, animated: true) {
                    self.audioService.startAlarmSound()
                }
            }
            return
        default:
            break
        }
        
        completionHandler([])
    }
    
    // click on local notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        Log.debug.log("Scheduler userNotificationCenter didReceive response")

        let actionIdentifier = response.actionIdentifier
        let userInfo = response.notification.request.content.userInfo
        
        Log.debug.log(actionIdentifier)
        Log.debug.log(userInfo)
        if self.audioService.state != .alarm {
            self.audioService.state = .alarm
        }
        switch actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            Log.info.log("Default Action")
            self.audioService.pauseSleepSession()
        case UNNotificationDismissActionIdentifier:
            Log.info.log("Dismiss Action")
            self.audioService.pauseSleepSession()
        default:
            break
        }
        completionHandler()
    }
}
