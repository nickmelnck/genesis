//
//  AudioService.swift
//  Genesis
//
//  Created by Nick Melnick on 09.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation
import AVFoundation

enum AudioServiceState {
    case idle
    case calming
    case tracking
    case sleep
    case alarm
    case pause
    
    var asString: String {
        switch self {
        case .idle: return "Idle".localized
        case .calming: return "Calming sounds".localized
        case .tracking: return "Snore & Sleep tracking".localized
        case .sleep: return "Sleeping".localized
        case .alarm: return "Alarm".localized
        case .pause: return "Pause".localized
        }
    }
}

protocol AudioServiceDelegate: class {
    
    func didChangeState(service: AudioService)
    
    func didParamsChanged(service: AudioService)
    
}

extension AudioServiceDelegate {
    
    func didChangeState(service: AudioService) { }
    
    func didParamsChanged(service: AudioService) { }
    
}

class AudioService: NSObject, MultiDelegates {
    
    private let observerInterval: TimeInterval = 1
    
    let calmingSound = SoundFile(name: "Nature", soundDescription: "", filename: "nature.m4a", type: .calming)
    let alarmSound = SoundFile(name: "Alarm", soundDescription: "", filename: "alarm.m4a", type: .alarm)
    
    var delegates = WeakArray()
    var state: AudioServiceState = .idle {
        willSet {
            if state != newValue && newValue == .pause {
                currentState = state
            }
        }
        didSet {
            Log.info.log("State is \(state.asString)")
            if state == .idle {
                resetService()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    self?.deactivateAudioSession()
                }
            }
            if oldValue != state {
                delegates.compactMap{ $0.value as? AudioServiceDelegate }.forEach({
                    $0.didChangeState(service: self)
                })
            }
        }
    }
    private var currentState: AudioServiceState = .idle {
        didSet {
            Log.info.log("Current paused state is \(currentState.asString)")
        }
    }
    
    var calmingSoundDuration: TimeInterval = 15.0 * 60 {
        didSet {
            Constants.Params.calmingSoundDuration.setValue(calmingSoundDuration)
            delegates.compactMap{ $0.value as? AudioServiceDelegate }.forEach({
                $0.didParamsChanged(service: self)
            })
        }
    }
    var isCalmingSoundEnabled:Bool { return calmingSoundDuration > 0.0 }
    
    var isTrackingEnabled: Bool = true {
        didSet {
            Constants.Params.isTrackingEnabled.setValue(isTrackingEnabled)
            delegates.compactMap{ $0.value as? AudioServiceDelegate }.forEach({
                $0.didParamsChanged(service: self)
            })
        }
    }
    var isAlarmEnabled: Bool = true {
        didSet {
            Constants.Params.isAlarmEnabled.setValue(isAlarmEnabled)
            delegates.compactMap{ $0.value as? AudioServiceDelegate }.forEach({
                $0.didParamsChanged(service: self)
            })
        }
    }
    var alarmTime: TimeInterval = 0.0 {
        didSet {
            Constants.Params.alarmTime.setValue(alarmTime)
            delegates.compactMap{ $0.value as? AudioServiceDelegate }.forEach({
                $0.didParamsChanged(service: self)
            })
        }
    }
    
    fileprivate var player: AVQueuePlayer?
    fileprivate var looper: AVPlayerLooper?
    
    private var timeObserverToken: Any?
    
    private var calmingTimer: TimeInterval = 0.0
    
    private let session = AVAudioSession.sharedInstance()
    private var isAudioSessionActive = false
    
    private var avAudioEngine = AVAudioEngine()
    
    private var recordingFormat: AVAudioFormat!
    private var recordingFile: AVAudioFile?
    public private(set) var isRecording = false

    
    override init() {
        super.init()
        restoreDefaults()
        do {
            try session.setCategory(.playAndRecord,
                                    mode: .measurement,
                                    options: [.interruptSpokenAudioAndMixWithOthers, .allowBluetooth, .allowBluetoothA2DP])
            try session.overrideOutputAudioPort(.speaker)
            let input = avAudioEngine.inputNode
            recordingFormat = input.outputFormat(forBus: 0)
            
            input.installTap(onBus: 0, bufferSize: 256, format: recordingFormat) { [unowned self] (buffer, when) in
                if self.isRecording {
                    guard let file = self.recordingFile else {
                        Log.error.log("Recording fils is nil")
                        return
                    }
                    do {
                        try file.write(from: buffer)
                    } catch {
                        Log.error.log("Error write to file with error: \(error)")
                    }
                }
            }
        } catch {
            Log.error.log(error)
        }
        avAudioEngine.prepare()
        subscribeToInterruptions()
    }
    
    deinit {
        unsubscribeFromInterruptions()
        avAudioEngine.stop()
    }
    
    private func restoreDefaults() {
        calmingSoundDuration = Constants.Params.calmingSoundDuration.getValue() as? Double ?? (15.0 * 60)
        isTrackingEnabled = Constants.Params.isTrackingEnabled.getValue() as? Bool ?? true
        isAlarmEnabled = Constants.Params.isAlarmEnabled.getValue() as? Bool ?? true
        let defaultAlarmValue = Calendar.current.date(bySettingHour: 8, minute: 0, second: 0, of: Date(), matchingPolicy: .nextTime, repeatedTimePolicy: .first, direction: .forward)?.timeIntervalSince1970 ?? Date().timeIntervalSince1970
        alarmTime = Constants.Params.alarmTime.getValue() as? Double ?? defaultAlarmValue
    }
    
    @discardableResult
    private func activateAudioSession() -> Bool {
        guard isAudioSessionActive == false else { return true }
        do {
            try self.session.setActive(true, options: [])
            isAudioSessionActive = true
            return true
        } catch {
            Log.error.log(error)
            isAudioSessionActive = false
            return false
        }
    }
    
    private func deactivateAudioSession() {
        defer {
            isAudioSessionActive = false
        }
        guard isAudioSessionActive == true else {
            return
        }
        do {
            try self.session.setActive(false, options: [.notifyOthersOnDeactivation])
        } catch {
            Log.error.log(error)
        }
    }
    
    func startSleepSession() {
        activateAudioSession()
        if isTrackingEnabled {
            session.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if !allowed {
                        self.isTrackingEnabled = false
                    }
                }
            }
        }
        if isTrackingEnabled {
            checkEngineIsRunning()
        }
        switch (state, currentState) {
        case (.idle, _):
            if self.isCalmingSoundEnabled {
                self.startCalmingSound()
            } else if self.isTrackingEnabled {
                self.startTracking()
            } else {
                self.state = .sleep
            }
            break
        case (.pause, .calming):
            self.startCalmingSound()
            break
        case (.pause, .tracking):
            self.startTracking()
            break
        case (.pause, .sleep):
            if self.isTrackingEnabled {
                startTracking()
            }
        default:
            break
        }
    }
    
    func pauseSleepSession() {
        guard self.state != .idle else { return }
        guard state != .alarm else {
            stopPlaying()
            self.state = .idle
            return
        }
        if self.player?.rate == 1 {
            player?.pause()
        }
        if avAudioEngine.isRunning {
            avAudioEngine.pause()
        }
        if state == .sleep && !isAlarmEnabled {
            state = .idle
        } else {
            self.state = .pause
        }
    }
    
    func startCalmingSound() {
        if self.player == nil {
            createPlayerWith(file: self.calmingSound)
            addTimeObserverTo()
            calmingTimer = self.calmingSoundDuration
        }
        self.player?.play()
        self.state = .calming
    }
    
    func startTracking() {
        checkEngineIsRunning()
        if self.recordingFile == nil {
            do {
                self.recordingFile = try AVAudioFile(forWriting: getFileURL(), settings: recordingFormat.settings)
            } catch {
                Log.error.log("Error create file: \(error)")
            }
        }
        self.isRecording = true
        self.state = .tracking
    }
    
    func startAlarmSound() {
        player?.pause()
        isRecording = false
        if avAudioEngine.isRunning {
            avAudioEngine.stop()
        }
        createPlayerWith(file: self.alarmSound)
        self.player?.play()
        self.state = .alarm
    }
    
    private func stopPlaying() {
        removeTimeObserver()
        self.looper?.disableLooping()
        self.player?.pause()
        avAudioEngine.stop()
        calmingTimer = 0
        self.looper = nil
        self.player = nil
    }
    
    private func createPlayerWith(file: SoundFile) {
        removeTimeObserver()
        if let url = file.url {
            let item = AVPlayerItem(asset: AVAsset(url: url))
            let queuePlayer = AVQueuePlayer(playerItem: item)
            self.player = queuePlayer
            self.looper = AVPlayerLooper(player: queuePlayer, templateItem: item)
        }
    }
    
    private func addTimeObserverTo() {
        guard let player = self.player else { return }

        let interval = CMTime(seconds: observerInterval, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        self.timeObserverToken = player.addPeriodicTimeObserver(forInterval: interval, queue: .main, using: { [weak self] (time) in
            guard let me = self else { return }
            me.calmingTimer -= me.observerInterval
            if me.calmingTimer <= 0 {
                me.removeTimeObserver()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    guard let `self` = self else { return }
                    self.stopPlaying()
                    if self.isTrackingEnabled {
                        self.startTracking()
                    } else if self.isAlarmEnabled {
                        self.state = .sleep
                    } else {
                        self.state = .idle
                    }
                }
            }
        })
    }
    
    private func removeTimeObserver() {
        guard let player = self.player, let token = self.timeObserverToken else {
            self.timeObserverToken = nil
            return
        }
        player.removeTimeObserver(token)
        self.timeObserverToken = nil
    }
    
    func subscribeToInterruptions() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleInterruption),
                                               name: AVAudioSession.interruptionNotification,
                                               object: nil)
    }
    
    func unsubscribeFromInterruptions() {
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.interruptionNotification, object: nil)
    }
    
    func checkEngineIsRunning() {
        if !avAudioEngine.isRunning {
            start()
        }
    }
    
    func start() {
        do {
            try avAudioEngine.start()
        } catch {
            print("Could not start audio engine: \(error)")
        }
    }
    
    private func resetService() {
        isRecording = false
        self.recordingFile = nil
        avAudioEngine.stop()
        self.player = nil
        self.looper = nil
        calmingTimer = 0
    }
    
    @objc func handleInterruption(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
            let type = AVAudioSession.InterruptionType(rawValue: typeValue)
            else {
                return
        }
        
        switch type {
            
        case .began:
            Log.debug.log("Interruption of audiosession started")
            if state == .calming || state == .tracking {
                self.pauseSleepSession()
            }
        case .ended:
            Log.debug.log("Interruption of audiosession ended")
            guard let optionsValue = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else { return }
            let options = AVAudioSession.InterruptionOptions(rawValue: optionsValue)
            if options.contains(.shouldResume) {
                if state == .pause {
                    self.startSleepSession()
                }
            } else {
                pauseSleepSession()
            }
            
        default: ()
        }
        
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    private func getFileURL() -> URL {
        let path = getDocumentsDirectory().appendingPathComponent("recording.caf")
        return path as URL
    }
    
}

